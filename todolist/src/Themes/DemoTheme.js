import React, {Component} from 'react' 
import styled,{ThemeProvider} from 'styled-components'


const configDarkTheme={
    background: '#000000',
    color:'#ffffff',
}

const configLightTheme={
   background:'#ffffff',
   color:'#000000',
}

 export default class DemoTheme extends Component {

    state={
        currentTheme: configDarkTheme
    }
    handleChangeTheme=(e)=>{
        this.setState({
            currentTheme: e.target.value=='1'? configDarkTheme : configLightTheme
        })
    } 
     render() {  
    const DivStyle= styled.div`
        color:${props=> props.theme.color};
        padding: 5%;
        background-color: ${props=> props.theme.background}
    `

    return (
        <ThemeProvider theme={this.state.currentTheme}>
        <DivStyle>
           CyberSoft
        </DivStyle>
        <select onChange={this.handleChangeTheme}>
            <option value="1">Dark theme</option>
            <option value="2">Light theme</option>
        </select>
        </ThemeProvider>
    )
     }
 }
 

// const DemoTheme = (props) => {

//     const configDarkTheme={
//          background: '#000000',
//          color:'#ffffff',
//     }

//     const configLightTheme={
//         background:'#ffffff',
//         color:'#000000',
//     }

//     const DivStyle= styled.div`
//         color:${props=> props.theme.color};
//         padding: 5%;
//         background-color: ${props=> props.theme.background}
//     `

//     return (
//         <ThemeProvider theme={configDarkTheme}>
//         <DivStyle>
//             Hello, VU DUC HAI
//         </DivStyle>
//         <select >
//             <option>Dark theme</option>
//             <option>Light theme</option>
//         </select>
//         </ThemeProvider>
//     )
// }

// export default DemoTheme