export const ToDoListLightTheme ={
    bgColor: '#fff',
    color: '#7952b3',
    boderButton: '1px solod #7952b3',
    borderRadiusButton: 'none',
    hoverTextColor: '#343a40',
    hoverBgColor:'#7952b3', 
    borderColor:'#7952b3',
}