/* eslint-disable import/no-anonymous-default-export */
import { DarkTheme } from "../../Themes/DarkThemes"
import { LightTheme } from "../../Themes/LightThemes"
import { PrimaryTheme } from "../../Themes/PrimaryThemes"
import { add_task, change_theme, delete_task, done_task, edit_task, update_task } from "../Types/toDoListTypes"
import {arrTheme} from './../../Themes/mainTheme'
const initialState = {
    themeToDoList: DarkTheme,
    taskList:[
        {id:'task-1',taskName: 'task1', done: true},
        {id:'task-2',taskName: 'task2', done: false},
        {id:'task-3',taskName: 'task3', done: false},
        {id:'task-4',taskName: 'task4', done: true},
        {id:'task-5',taskName: 'task5', done: true},
    ],
    taskEdit:{id: '-1', taskName: '', done: false}
}

 
export default (state = initialState, action) => {
    switch (action.type) {
    case add_task:{
        // console.log('reducer', action.newTask);
        //Kiểm tra rỗng
        if(action.newTask.taskName.trim=== ''){
            alert('Task name is required!')
            return {...state};
        }
        // Kiểm tra tồn tại
        let taskListUpdate=[...state.taskList];
        let index= taskListUpdate.findIndex((task=>task.taskName=== action.newTask.taskName))
        if(index !==-1){
            alert('Task name is alredy exist!!!');
            return {...state};
        }
        taskListUpdate.push(action.newTask);
        // Xử lý xong thì gán taskList mới vào taskList
        state.taskList= taskListUpdate;
        return {...state};
    }
    case change_theme:{
        let objTheme= arrTheme.find(theme=>theme.id==action.themeId);
        // console.log(objTheme);
        // console.log(action);
        if(objTheme){
            state.themeToDoList={...objTheme.theme};
        }
        return {...state};
    }
 
    case done_task:{
         
        let taskListUpdate= [...state.taskList];

        let index= taskListUpdate.findIndex(task=> task.id===action.taskId);

        if(index!==-1){
            taskListUpdate[index].done= true;
        }

        // state.taskList= taskListUpdate;

        return {...state, taskList: taskListUpdate}
    }

    case delete_task:{
        // console.log(action);
        let taskListUpdate= [...state.taskList];
        
        taskListUpdate= taskListUpdate.filter(task=> task.id !== action.taskId);

        return {...state, taskList: taskListUpdate}
    }

    case edit_task:{


        return{...state, taskEdit: action.task}
    }

    case update_task:{
        //console.log(action.taskName);
        // chỉnh sửa lại taskName của taskEdit
        state.taskEdit= {...state.taskEdit, taskName:action.taskName, } 
        //tìm trong taskList cập nhật lại taskEdit người dùng update
        let taskListUpdate= [...state.taskList];
        let index=taskListUpdate.findIndex(task=> task.id === state.taskEdit.id );

        if(index !==-1){
            taskListUpdate[index]= state.taskEdit;
        }
         
        state.taskList= taskListUpdate;
        state.taskEdit={id: '-1', taskName:'', done: false}
        return {...state};
    }

    default:
        return {...state}
    }
}
