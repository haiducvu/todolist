import {combineReducers} from 'redux';
import todoListReducer from './toDoListReducer';
export const rootReducer=combineReducers({
    todoListReducer
})