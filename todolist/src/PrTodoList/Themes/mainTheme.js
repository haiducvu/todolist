import { DarkTheme } from "./DarkThemes";
import { LightTheme } from "./LightThemes";
import { PrimaryTheme } from "./PrimaryThemes";

export const arrTheme=[
    {
        id:1,
        name:'DarkTheme',
        theme: DarkTheme,
    },
    {
        id:2,
        name:'LightTheme',
        theme: LightTheme,
    },
    {
        id:3,
        name:'PrimaryTheme',
        theme: PrimaryTheme,
    },
]