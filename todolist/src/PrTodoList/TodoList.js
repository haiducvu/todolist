import React, { Component } from 'react';
import { Container } from './Components/Containers';
import { ThemeProvider } from 'styled-components';
// import { DarkTheme } from './Themes/DarkThemes';
// import { LightTheme } from './Themes/LightThemes';
// import { PrimaryTheme } from './Themes/PrimaryThemes';
import { Dropdown } from './Components/Dropdown';
import {
  // Heading1,
  // Heading2,
  Heading3,
  // Heading4,
  // Heading5,
} from './Components/Heading';
import { TextField  } from './Components/TextField';
import { Button } from './Components/Button';
import { Table, Tr, Th, Thead } from './Components/Table';
import {
  addTaskAction,
  changeThemeAction,
  deleteTaskAction,
  doneTaskAction,
  editTaskAction,
  updateTaskAction,
} from './../PrTodoList/Redux/Actions/toDoListAction';
import { arrTheme } from './Themes/mainTheme';
import { connect } from 'react-redux';
// import { delete_task } from './Redux/Types/toDoListTypes';
class TodoList extends Component {
  state = { taskName: '', disabled: true };

  renderTaskToDo = () => {
    return this.props.taskList
      .filter((task) => !task.done)
      .map((task, index) => {
        return (
          <>
            <Tr key={index}>
              <Th>{task.taskName}</Th>
              <Th className='text-right'>
                <Button
                  onClick={() => {
                   this.setState({
                      disabled: false,
                   },()=>{
                    this.props.dispatch(editTaskAction(task));
                   })
                  }}
                  className='ml-2'>
                  Edit
                </Button>
                <Button
                  onClick={() => {
                    this.props.dispatch(doneTaskAction(task.id));
                  }}
                  className='ml-2'>
                  Check
                </Button>
                <Button
                  onClick={() => {
                    this.props.dispatch(deleteTaskAction(task.id));
                  }}
                  className='ml-2'>
                  Trash
                </Button>
              </Th>
            </Tr>
          </>
        );
      });
  };

  renderTaskToComplete = () => {
    return this.props.taskList
      .filter((task) => task.done)
      .map((task, index) => {
        return (
          <>
            <Tr key={index}>
              <Th>{task.taskName}</Th>
              <Th className='text-right'>
                <Button
                  onClick={() => {
                    this.props.dispatch(deleteTaskAction(task.id));
                  }}
                  className='ml-2'>
                  Trash
                </Button>
              </Th>
            </Tr>
          </>
        );
      });
  };

  renderTheme = () => {
    return arrTheme.map((theme, index) => {
      return <option value={theme.id}>{theme.name}</option>;
    });
  };

  // LifeCycle ver 16 nhận vào props mới được thực thi trước render
  // componentWillReceiveProps(newProps){
  //   console.log('this.props',this.props);
  //   console.log('newProps', newProps);

  //   this.setState({
  //     taskName: newProps.taskEdit.taskName,
  //   })
  // }

  //LifeCycle tĩnh không truy xuất được con trỏ this
  //static getDerivedStateFromProps(newProps, currentState){
  // newProps là props mới, props cũ là this.props( không truy xuất được)
  // currentState: ứng với state hiện tại this.state

  // // hoặc trả về state mới( this.state)
  // let newState={...currentState, taskName: newProps.taskEdit.taskName}
  // return newState;

  //trả về null state giữ nguyên
  // reutrn null;
  //}

  render() {
    return (
      <ThemeProvider theme={this.props.themeToDoList}>
        <Container className='w-50'>
          <Dropdown
            onChange={(e) => {
              // console.log(e.target.value);
              let { value } = e.target;
              //Dispatch value len reducer
              this.props.dispatch(
                // {
                // type:'change_theme',
                // themeId: value,
                // }
                changeThemeAction(value)
              );
            }}>
            {/* <option>Dark Theme</option>
            <option>Light Theme</option>
            <option>Primary Theme</option> */}
            {this.renderTheme()}
          </Dropdown>
          <Heading3>To Do List</Heading3>
          {/* lấy thông tin thông qua propperity 'name'+ fn OnClick của TextField */}
          <TextField
            value={this.state.taskName}
            onChange={(e) => {
              this.setState({
                // [e.target.name]: e.target.value //C1
                taskName: e.target.value, //C2
              }); //,()=>{console.log(this.state)}
            }}
            name='taskName'
            label='Task Name'
            className='w-50'></TextField>
      
         <Button
            onClick={() => {
              // Lấy thông tin from input user
              let { taskName } = this.state;
              //Tạo ra 1 task object
              let newTask = {
                id: Date.now(),
                taskName: taskName,
                done: false,
              };
              // console.log('TASK',task);
              //Đưa task object lên redux thông qua phương thức dispatch  
               if(this.state.taskName!==''){
                this.props.dispatch(addTaskAction(newTask)); 
               }
            }}
            className='ml-2'>
            <i className='fa fa-plus'>Add Task</i>
          </Button> 
          {/* <Button
            onClick={() => {
              // Lấy thông tin from input user
              let { taskName } = this.state;
              //Tạo ra 1 task object
              let newTask = {
                id: Date.now(),
                taskName: taskName,
                done: false,
              };
              // console.log('TASK',task);
              //Đưa task object lên redux thông qua phương thức dispatch 
              if(this.state.taskName!==''){
                this.props.dispatch(addTaskAction(newTask));
              }
            }}
            className='ml-2'>
            <i className='fa fa-plus'>Add Task</i>
          </Button> */}
           {this.state.disabled  ? (
           <Button
              disabled
              onClick={() => {
                this.props.dispatch(updateTaskAction(this.state.taskName));
              }}
              className='ml-2'>
              <i className='fa fa-upload'>Update Task</i>
            </Button>
          ) : (
            <Button
              onClick={() => {
                let {taskName}= this.state;
               this.setState({
                 disabled: true,
                 taskName:''
               }, ()=>{
                this.props.dispatch(updateTaskAction(taskName));
               })
              }}
              className='ml-2'>
              <i className='fa fa-upload'>Update Task</i>
            </Button>
          )}

          <hr />
          <Heading3>Task To Do</Heading3>
          <Table>
            <Thead>{this.renderTaskToDo()}</Thead>
          </Table>
          <hr />
          <Heading3>Task Complete</Heading3>
          <Table>
            <Thead>{this.renderTaskToComplete()}</Thead>
          </Table>
        </Container>
      </ThemeProvider>
    );
  }

  // Đây là lifecycle trả về props cũ và state cũ của component trước khi render (nhưng chạy sau render)
  componentDidUpdate(prevProps, prevState) {
    // So sánh nếu như props trước đó( taskEdit trước mà khác taskEdit hiện tại thì mới setState)
    if (prevProps.taskEdit.id !== this.props.taskEdit.id) {
      this.setState({
        taskName: this.props.taskEdit.taskName,
      });
    }
  }
}

const mapStateToProps = (state) => {
  return {
    themeToDoList: state.todoListReducer.themeToDoList,
    taskList: state.todoListReducer.taskList,
    taskEdit: state.todoListReducer.taskEdit,
  };
};

export default connect(mapStateToProps)(TodoList);
