// method filter => trả về mảng cần tìm
// method findIndex=> trả về vị trị cần tìm
// method find=> trả về object ĐẦU TIÊN cần tìm 

// CÁC HÀM XỬ LÝ ARRAY
// forearch=> là phương thức thực thi 1 hàm 1 lần cho 1 phần tử  
    // foreach=>dùng để duyệt mảng, ko TRẢ VỀ giá trị, ko RETURN=> undifined

// map khác foreach là return về giá trị, khác với fiter là duyệt tất cả các phần tử trong mảng, 
//  nếu phần tử nào không thỏa điều kiện thì trả undifined    

// reduce=> là phương thức thực thi n lần với n phần tử Để tạo ra 1 giá trị mới: return về variable, obj, array,.. 


var courses=[
    {
        name: 'nguyenvana',
        coin: 600,
        stt: 0,
    },
    {
        name: 'nguyenvanb',
        coin: 200,
        stt: 1,
    },
    {
        name: 'nguyenvanc',
        coin: 399,
        stt: 2,
    },
    {
        name: 'nguyenvand',
        coin: 600,
        stt: 3,
    },
    {
        name: 'nguyenvane',
        coin: 0,
        stt: 4,
    },
    {
        name: 'nguyenvanf',
        coin: -151,
        stt: 5,
    },
    {
        name: 'nguyenvanh',
        coin: -151,
        stt: 2,
    }
]
// filter
                //C1 viet function binh thuong
// var filterCourse= courses.filter(function(course, index, array){     
//     return course.coin>200;
// })
                //C2 viet arrow function 
// var filterCourse= courses.filter((course)=> {return course.coin>200});
                //C3 viet arrow function neu chi co 1 line
var filterCourse= courses.filter(course=>  course.coin>200);

console.log('Filter',filterCourse);

// findIndex
var indexCourse= courses.findIndex(index=> index.stt===4);
console.log('FindIndex', indexCourse);
console.log('ObjectFindIndex', courses[indexCourse]);

// find 
var findCourse= courses.find(indexStt=> indexStt.stt===2);
console.log('FindCourse', findCourse); // nếu không tìm thấy trả về undifined

//foreach => dùng để duyệt mảng, ko trả về, ko return=> undifined
var foreachCourse= courses.forEach((coin, index)=>{ 
    console.log('FOREACH', coin);
    // return coin;
})

// console.log('FOREACH', foreachCourse);

// reduce
let sumCoin= courses.reduce((sum, item, index)=> {
    return sum += item.coin;
},0)

console.log('SUM COIN', sumCoin);

