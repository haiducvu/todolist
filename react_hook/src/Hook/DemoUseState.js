import React, {useState} from 'react'

const DemoUseState = (props) => {
    //classComponent
    //(1)this.state={}
    //(2)this.setState(newState);
    let [state, setState]= useState({like: 0});

    const handleButton=()=>{
        setState({
            like: state.like +1
        })
        //console.log state sẽ bị trễ vì setState là hàm bất đồng bộ, chạy lại hết hàm
        // console.log("State", state);
    }

    return (
        <div>
             <p>{state.like} TYM</p>
             <button style={{width: '50px', height:'50px'}} onClick={handleButton}>+</button>
        </div>
    )
}

export default DemoUseState
