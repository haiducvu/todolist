import React, {useState, useEffect} from 'react'

function ChildUseEffect() {
    let [number, setNumber]= useState(1);
    console.log("render ChildUseEffect");
    useEffect(() => {
        //viet cho didupdate
        console.log('didunmount');
        return () => {
            console.log('willunmount');
        }
    }, [number])
    return (
        <div>
            <textarea></textarea> <br/>
            <button>Send</button>
        </div>
    )
}

export default ChildUseEffect
