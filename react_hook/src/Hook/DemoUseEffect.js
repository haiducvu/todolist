import React, {useState, useEffect} from 'react'
import ChildUseEffect from './ChildUseEffect';

function DemoUseEffect(props) {

    let [number, setNumber]= useState(1);

    //use effect là hàm chạy sau render thay cho didupdate va did mount trong mọi trường hợp
    //C1 thay thế cho componentdidmount
    // useEffect(()=>{
    //     console.log('didmount');
    //     console.log('didupdate');
    // })
    // console.log('render');

    //C2 Cách viết thay thế cho componentdimount
    useEffect(()=>{
        console.log('didmount');
    },[])
    
    //C3 Cách viết thay thế cho componentdidupdate
    useEffect(()=>{
        console.log('didupdate khi number thay đổi');
    },[number]); // number bị thay đổi sau render thì useEffect này sẽ chạy
    

    //C4 Cách viết thay thế cho componentWillUnMomount=> nghĩa là component này xuất hiện or ko xuất hiện trên giao diện
    console.log('render');

    const handleIncrease=()=>{
        setNumber(number+1)
    }

    
  

    return (
        <div>
            <button className="btn btn-success" onClick={handleIncrease}>Like</button>
            <h4>{number}</h4>
            {number ===1 ? <ChildUseEffect/> : ''}
        </div>
    )
}

export default DemoUseEffect
