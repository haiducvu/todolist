import React, {useState, memo, useCallback} from 'react'
import ChillUseCallback from './ChillUseCallback';

export default function DemoUseCallback(props) {
    let [state, setState]= useState({like: 0});
    const handleButton=()=>{
        setState({
            like: state.like +1
        })
    }
    const renderNotify=()=>{
        return `Đã thả ${state.like} tym!!!!`
    }

    const callBackrenderNotify= useCallback(renderNotify, [])

    return (
        <div>
            <h3>Use CallBack</h3>
        <p>{state.like} TYM</p>
        <button style={{width: '50px', height:'50px'}} onClick={handleButton}>+</button>

        <small>{renderNotify()}</small>
        <ChillUseCallback item={callBackrenderNotify}/>
   </div>
    ) 
}
 