import React, {useReducer} from 'react'

const initialCart=[
    // {id:1, name:'iPhone1', price: 10001, quantity: 1}, 
]

const cartReducer= (state, action)=>{

    // eslint-disable-next-line default-case
    switch (action.type) {
      case 'addToCard': {
        let carUpdate = [...state];

        let index= carUpdate.findIndex(itemCart=> itemCart.id=== action.item.id)
        if(index!==-1){
            carUpdate[index].quantity+=1;
        }else{
            const itemCart= {...action.item, quantity:1}; 
            carUpdate.push(itemCart);
        }
        return carUpdate;
      }
      
    }

    return [...state]
}

let arrProduct=[
    {id:1, name:'iPhone1', price: 10001, quantity: 1},
    {id:2, name:'iPhone2', price: 10002, quantity: 1},
    {id:3, name:'iPhone3', price: 10003, quantity: 1},
    {id:4, name:'iPhone4', price: 10004, quantity: 1},
    {id:5, name:'iPhone5', price: 10005, quantity: 1},
    {id:6, name:'iPhone6', price: 10006, quantity: 1},
]


export default function DemoUseReducer(props) {

    let[cart, dispatch]= useReducer(cartReducer, initialCart)

    const addToCard=(itemCard)=>{
        // console.log(itemCard);
        const action={
            type: 'addToCard',
            item: itemCard,
        }
        dispatch(action);
    }
 
    return (
        <div>
            <h3>DemoUseReducer</h3>
            <h4>Sản Phẩm</h4>
            <div>
                {arrProduct.map((item,index)=>{
                   return <div>
                       <span>{item.name}</span>
                       <span>{item.price}</span>
                       <span><button onClick={()=> {addToCard(item)}}>+</button></span>
                   </div>                
                })}
            </div>
        

            <h4>Giỏ hàng</h4>
            <table>
                <thead>
                    <tr>
                        <th>id</th>
                        <th>name</th>
                        <th>price</th>
                        <th>quantity</th>
                        <th>total</th>
                    </tr>
                </thead>
                <tbody>
                    {cart.map((item,index)=>{
                        return <tr key={index}>
                                    <td>{item.id}</td>
                                    <td>{item.name}</td>
                                    <td>{item.price}</td>
                                    <td>{item.quantity}</td> 
                                    <td>{item.quantity* item.price}</td>
                                    <td><button>-</button></td> 
                                </tr>
                    })}
                </tbody>
            </table>
        </div>
    )
}
