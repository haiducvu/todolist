import React, {memo} from 'react'

 function ChillUseCallback(props) {
    console.log('ChillUseCallBack');
    return (
        <div>
            <div>ChillCallback</div>
            <div>{props.item()}</div>
        </div>
    )
}
export default memo(ChillUseCallback);