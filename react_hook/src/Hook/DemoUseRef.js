import React,{useState, useRef} from 'react'

export default function DemoUseRef(props) {

    let inputUserName= useRef(null);
    let inputPassword= useRef(null);

    let userName= useRef('');

    let[userLogin, setUserLogin]= useState({userName:''});

    const handleLogin=()=>{
        // console.log(inputUserName.current.value);
        // console.log(inputPassword.current.value);
        
        console.log(userName.current);
        console.log(userLogin.userName);
        userName.current='abc';
        setUserLogin({
            userName:userName.current,
        })
    }

    return (
        <div>
            <h3>DemoUseRef</h3>
            <div className='form-group'>
                <h3>Username</h3>
                <input ref={inputUserName} name='userName' className='form-control'/> 
            </div>
            <div className='form-group'>
                <h3>Password</h3>
                <input ref={inputPassword} name='passWord' className='form-control'/> 
            </div>
            <div className='form-group'> 
                <button className='btn btn-success' onClick={()=>{handleLogin()}}>Login</button>
            </div>
        </div>
    )
}
