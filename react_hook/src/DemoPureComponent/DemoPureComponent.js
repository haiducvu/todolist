import React, { Component } from 'react'
import Profile from './Profile'

export default class DemoPureComponent extends Component {

    state={
       like: 0,  
    }

    likeButton=()=>{
        this.setState({
            like: this.state.like+1,
        })
    }

    render() {
            return (  
           <div className="container">
               <Profile item={this.state.like}/>
                <div className="card text-white bg-primary"> 
                 <div className="card-body">
                <h4 className="card-title">PureComponent Like({this.state.like})</h4> 
                <button onClick={()=>{this.likeButton()}} style={{width: '90px', height:'50px', marginLeft: '20px'}}>Press</button>
            </div>
            </div> 
           </div>
        )
    }
}


// =>  PureComponent ko nên lạm dụng,  bản chất của PC là tự động kiểm tra xem nếu props và state của component
// đó  thay đổi thì sẽ render lại. Nhưng sự so sánh thay đổi của react là so sánh tham chiếu(shallow comparision- so sánh nguyên thủy),
// nếu như ta truyền  1 object dưới dạng props, và thay đổi thuộc tính nào đó thì react sẽ không so sánh được, vì căn bản vẫn là 1 object
// => nếu ta muốn force thì đối với object, tạo ra bản sao của object đó [...object]