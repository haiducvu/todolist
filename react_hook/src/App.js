import DemoPureComponent from "./DemoPureComponent/DemoPureComponent";
import DemoUseCallback from "./Hook/DemoUseCallback";
import DemoUseEffect from "./Hook/DemoUseEffect";
import DemoUseReducer from "./Hook/DemoUseReducer";
import DemoUseRef from "./Hook/DemoUseRef";
import DemoUseState from "./Hook/DemoUseState";

 
function App() {
  return (
    <div > 
      {/* <DemoUseState/> */}
      {/* <DemoUseEffect/> */}
      {/* <DemoPureComponent/> */}
      {/* <DemoUseCallback/> */}
      {/* <DemoUseRef/> */}
      <DemoUseReducer/>
    </div>
  );
}

export default App;
